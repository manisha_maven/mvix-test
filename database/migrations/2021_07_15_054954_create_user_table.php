<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->bigInteger('client_id')->unsigned()->index();              
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 150)->unique()->index()->comment('User unique email address.');;
            $table->string('password',256); 
            $table->string('phone', 20);
            $table->string('profile_url', 255)->nullable();
            $table->timestamp('last_password_reset')->default(now())->nullable(); 
            $table->enum('status', ['Active', 'Inactive'])->comment('User status Active|Inactive');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("client_id")->references("id")->on("client")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
