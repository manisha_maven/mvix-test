<?php

namespace Tests\Feature\Api\v1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class AccountTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        // $response = $this->get('/api/v1/account?sort_field=client_name&sort_type=desc&search_field=client_name&search_value=mav');

        // $response->assertStatus(200,$response->status());

        $this->json('get', 'api/v1/account?sort_field=client_name&sort_type=desc&search_field=client_name&search_value=mav')
         ->assertStatus(Response::HTTP_OK)
         ->assertJsonStructure(
             [
                 'data' => [
                     '*' => [
                         'id',
                         'name',
                         'address1',
                         'address2',
                         'city',
                         'state',
                         'country',
                         'zipcode',
                         'longitude',
                         'phoneNo1',
                         'phoneNo2',
                         'totalUser' => [
                                        'all',
                                        'active',
                                        'inactive'
                                    ],
                         'startValidity',
                         'endValidity',
                         'status',
                         'createdAt',
                         'updatedAt', 
                     ]
                 ]
             ]
         );
    }
}
