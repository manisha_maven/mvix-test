<?php

namespace Tests\Feature\Api\v1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    { 
        $this->json('POST', 'api/v1/register', [
                'client_name' => 'Mavin Surat',
                'client_email' => 'manisha@maven.one',
                'client_address1' => 'Mavin Address 1',
                'client_address2' => 'Mavin Address 2',
                'client_city' => 'Surat',
                'client_state' => 'Gujarat',
                'client_country' => 'dfgdfg',
                'client_zipcode' => '395007',
                'client_phone_no1' => '123-456-7897',
                'client_phone_no2' => '',
                'client_status' => 'Active',
                'user_firstname' => 'Manisha',
                'user_lastname' => 'Testing',
                'user_email' => 'manisha+m1@maven.one',
                'user_password' => '123456789',
                'user_phone' => '234-567-8976',
                'user_status' => 'Active'
            ])
            ->assertJsonStructure([
                "data" => [
                    'name',
                         'address1',
                         'address2',
                         'city',
                         'state',
                         'country',
                         'zipCode', 
                         'phoneNo1',
                         'phoneNo2',
                         "users" => [
                            "firstName",
                            "lastName",
                            "email",
                            "password",
                            "phone"
                            ]
                ],                 
            ]); 
    }
}


 
                         