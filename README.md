# MVIX - Client Account Self Registration

![https://laravel.com](https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg)

![Build Status](https://travis-ci.org/laravel/framework.svg)
![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)
![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)
![License](https://img.shields.io/packagist/l/laravel/framework)

### About Project
---

Laravel Test Project For Client Account Self Registration 

- Follow the installation steps to up and running.


## Installation Process
---

### Clone the BitBucket Repo.:
```
git clone REPO_URL
```

### Install composer packages: 
```
composer install

```
### Copy .env from .env.example and set Google Map API Key: 
cp .env.example .env

- Add Google MAP GEOCODING api key in variable `GOOGLE_MAPS_GEOCODING_API_KEY` in `.env` file
- Setup database credentials
```

```

### Generate Keys
```
php artisan key:generate

```

### Run Migrations for DB
```
php artisan migrate

```
### Run Server
```
php artisan serve

```


## Run Tests
```
php artisan test

```

# API EndPoints:

###  **POST**: `/api/v1/register` => To create an  client with user details.
1. Parameters 
```
client_name:Maven
client_email:maven+test@gmail.com
client_address1:SNS Atria,Opposit Jolly Party Plot
client_address2:Vesu
client_city:Surat
client_state:Gujarat
client_country:India
client_zipcode:395007
client_phone_no1:123-456-7897
client_phone_no2:
client_status:Active
user_firstname:Manisha
user_lastname:Baldaniya
user_email:manisha@maven.one
user_password:123456789
user_phone:234-567-8976
user_status:Active

```
- Success Response Data (JSON PostMan)

```
{
    "success": true,
    "statusCode": "200",
    "message": "Client & User created Successfully",
    "data": {
        "name": "Maven",
        "address1": "SNS Atria,Opposit Jolly Party Plot",
        "address2": "Vesu",
        "city": "Surat",
        "state": "Gujarat",
        "country": "India",
        "zipCode": "395007",
        "phoneNo1": "123-456-7897",
        "phoneNo2": null,
        "users": {
            "firstName": "Manisha",
            "lastName": "Baldaiya",
            "email": "manisha@maven.one",
            "password": "123456789",
            "phone": "234-567-8976"
        }
    }
}
```

### **GET**: `/api/v1/account` => To Fetch all client details.

- Pagination Parameter 

```
per_page = Number
```

- Sorting Paramenter
```
sort_field=client_name (field Name)
sort_type=desc (Sorting type ASC / DESC)
```

- Filtering Paramenter
```
search_field=client_name (field Name)
search_value= Any Value
```

- Example QueryString URL: `{{url}}/api/v1/account?sort_field=client_name&sort_type=desc&search_field=client_name&search_value=mav`

- Response Data (JSON PostMan)
```
{
    "data": [
        {
            "id": 2,
            "name": "Maven",
            "email": null,
            "address1": "SNS Atria,Opposit Jolly Party Plot",
            "address2": "Vesu",
            "city": "Surat",
            "state": "Gujarat",
            "country": "India",
            "zipcode": "395007",
            "latitude": 21.1820786,
            "longitude": 72.8405329,
            "phoneNo1": "123-456-7897",
            "phoneNo2": "",
            "totalUser": {
                "all": 1,
                "active": 1,
                "inactive": 0
            },
            "startValidity": "2021-07-16",
            "endValidity": "2021-07-31",
            "status": "Active",
            "createdAt": "2021-07-16T12:33:40.000000Z",
            "updatedAt": "2021-07-16T12:33:40.000000Z"
        }
    ],
    "links": {
        "first": "http://mvix-test.local/api/v1/account?page=1",
        "last": "http://mvix-test.local/api/v1/account?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://mvix-test.local/api/v1/account?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http://mvix-test.local/api/v1/account",
        "per_page": "10",
        "to": 1,
        "total": 1
    }
}
```