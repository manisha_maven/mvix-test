<?php

namespace App\Http\Requests\Api\v1;

use App\Http\Requests\BaseFormRequest;

class ClientRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_name' => 'required',
            'client_email' => 'required|email',
            'client_address1' => 'required',
            'client_address2' => 'required',
            'client_city' => 'required',
            'client_state' => 'required',
            'client_country' => 'required',
            'client_zipcode' => 'required|numeric',
            'client_phone_no1' => 'required|regex:/^\d{3}-\d{3}-\d{4}$/',
            'client_phone_no2' => 'nullable|regex:/^\d{3}-\d{3}-\d{4}$/',
            'client_status' => 'required',
            'user_firstname' => 'required',
            'user_lastname' => 'required',
            'user_email' => 'required|email|unique:App\Models\User,email,{$id}',
            'user_password' => 'required',
            'user_phone' => 'required|regex:/^\d{3}-\d{3}-\d{4}$/',
            'user_status' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'client_name.required'       => 'Please enter Client Name',
            'client_email.required'      => 'Please enter Client Email',
            'client_email.email'         => 'Please enter valid Client Email', 
            'client_address1.required'   => 'Please enter Client Address1.',
            'client_address2.required'   => 'Please enter Client Address2.',
            'client_city.required'       => 'Please enter Client City.',
            'client_state.required'      => 'Please enter Client State.',
            'client_country.required'    => 'Please enter Client Country.',
            'client_zipcode.required'    => 'Please enter Client ZipCode.',
            'client_zipcode.numeric'     => 'Please enter digits only in Client ZipCode.',
            'client_phone_no1.required'  => 'Please enter Client Phone No1.',
            'client_phone_no1.regex'     => 'Please enter Client Phone No1 in this format 555-666-7777.',
            'client_phone_no2.regex'     => 'Please enter Client Phone No1 in this format 555-666-7777.',
            'client_status.required'     => 'Please enter Client Status. Status must be eitther "Active" OR "Inactive".',
            'user_firstname.required'     => 'Please enter User Firstname.',
            'user_lastname.required'      => 'Please enter User Lastname.',
            'user_email.required'         => 'Please enter User Email',
            'user_email.email'            => 'Please enter valid User Email',
            'user_password.required'      => 'Please enter User Password',
            'user_phone.required'         => 'Please enter User Phone No.',
            'user_status.required'        => 'Please enter User Status. Status must be eitther "Active" OR "Inactive".',            
        ];
    }
}