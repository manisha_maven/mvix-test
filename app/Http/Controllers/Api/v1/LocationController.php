<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Geocoder\Geocoder;

class LocationController extends Controller
{
    //
    public function __construct()
    {
    }

    public function index()
    {
        $client = new \GuzzleHttp\Client();

        $geocoder = new Geocoder($client);
        
        $geocoder->setApiKey(config('geocoder.key'));
        
        $geocoder->setCountry(config('geocoder.country', 'ZA'));
        
       $data = $geocoder->getCoordinatesForAddress('Infinite Loop 1, Cupertino');
       print "<pre>";
       print_r($data);

    }
}