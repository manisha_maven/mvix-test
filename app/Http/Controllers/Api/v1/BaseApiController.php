<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

class BaseApiController extends Controller
{
     /* success response method. */

    public function sendResponse($result, $message,$statusCode = '200')
    {
        $response = [
            'success' => true,
            'statusCode' => $statusCode,
            'message' => $message,
            'data'    => $result            
       ];
 
       return response()->json($response);
    }

     /* return error response. */
    public function respondWithError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'statusCode' => $code,
            'message' => $error,
        ];
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response);
    }
}
