<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseApiController as BaseApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\ClientRequest;
use App\Http\Resources\Api\v1\Client\ClientCollection;
use App\Repository\Client\ClientRepository;
use App\Http\Requests\Api\v1\RegisterRequest;
use App\Http\Resources\Api\v1\RegisterResource;
use Illuminate\Http\Request;
use Spatie\Geocoder\Geocoder;

class ClientController extends BaseApiController
{
    private $clientRepository;
    public function __construct(ClientRepository $clientRepository)
    {
        $this->client = $clientRepository;
    }

    public function register(ClientRequest $request)
    {
        $attributes = array(
            'client_name' => $request->client_name,
            'client_email' => $request->client_email,
            'client_address1' => $request->client_address1,
            'client_address2' => $request->client_address2,
            'client_city' => $request->client_city,
            'client_state' => $request->client_state,
            'client_country' => $request->client_country,
            'client_zipcode' => $request->client_zipcode,
            'client_phone_no1' => $request->client_phone_no1,
            'client_phone_no2' => $request->client_phone_no2,
            'client_status' => $request->client_status, 
            'start_validity' =>  date('Y-m-d'),
            'end_validity' => date('Y-m-d', strtotime("+15 days")),
            'user_firstname' => $request->user_firstname,
            'user_lastname' => $request->user_lastname,
            'user_email' => $request->user_email,
            'user_password' => $request->user_password,
            'user_status' => $request->user_status,
            'user_phone' => $request->user_phone, 
        );

        // $client = new \GuzzleHttp\Client();
        // $geocoder = new Geocoder($client);
        // $geocoder->setApiKey(config('geocoder.key'));
        // $geocoder->setCountry(config('geocoder.country', 'ZA'));
        // if($request->client_address2 == '')
        //     $address = $request->client_address1.",".$request->client_city.",".$request->client_city.",".$request->client_state.",".$request->client_country.",".$request->client_zipcode;
        // else
        //     $address = $request->client_address1.",".$request->client_address2.','.$request->client_city.",".$request->client_city.",".$request->client_state.",".$request->client_country.",".$request->client_zipcode;

        // $address_detail  = $geocoder->getCoordinatesForAddress( $address);
        // $attributes['latitude'] = $address_detail['lat'] ? $address_detail['lat'] : '21.1820786';
        // $attributes['longitude'] = $address_detail['lng'] ? $address_detail['lng'] : '72.8405329';

        $attributes['latitude'] = '21.1820786';
        $attributes['longitude'] = '72.8405329';

        $data = $this->client->create($attributes);
        return $this->sendResponse(new RegisterResource($data),'Client has been successfully registered with User.'); 
    }
    public function getAccount(Request $request)
    { 
        $attributes['per_page'] = ($request->per_page ? $request->per_page : '10'); 
        $attributes['sort_field'] = ($request->sort_field ? $request->sort_field :  'created_at'); 
        $attributes['sort_type'] = ($request->sort_type ? $request->sort_type : 'desc');  
        $attributes['search_field'] = ($request->search_field ? $request->search_field : ''); 
        $attributes['search_value'] = ($request->search_value ? $request->search_value : '');  
        $data = $this->client->getAll($attributes);
        return new ClientCollection( $data );
        //return $this->sendResponse($result,'Accounts details retrieved.'); 
    }
}