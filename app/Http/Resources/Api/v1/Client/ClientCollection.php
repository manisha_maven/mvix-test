<?php

namespace App\Http\Resources\Api\v1\Client;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        $this->collection->transform(function ($data) {
            return new ClientResource($data);
        });

        return parent::toArray($request);       
    }
}
