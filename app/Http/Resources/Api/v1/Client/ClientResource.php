<?php

namespace App\Http\Resources\Api\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

       $createdDateTime = $this->createdAt;
       $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $createdDateTime)->format("Y-m-d\TH:i:s.u\Z");

       $updatedDateTime = $this->updatedAt;
       $updatedAt = Carbon::createFromFormat('Y-m-d H:i:s', $updatedDateTime)->format("Y-m-d\TH:i:s.u\Z");

       $phoneNo2 = '';
       if($this->phoneNo2 != null && $this->phoneNo2 != '')
       {
            $phoneNo2 = $this->phoneNo2;
       }

       return [ 
            'id'                     => $this->id, 
            'name'                   => $this->client_name,
            'email'                  => $this->client_email,
            'address1'               => $this->address1,
            'address2'               => $this->address2,
            'city'                   => $this->city,
            'state'                  => $this->state,
            'country'                => $this->country,
            'zipcode'                => $this->zipcode,
            'latitude'               => $this->latitude,
            'longitude'              => $this->longitude,
            'phoneNo1'               => $this->phoneNo1,
            'phoneNo2'               => $phoneNo2,
            'totalUser'              => array("all" => $this->user_count, "active" => $this->active_count, "inactive" => $this->inactive_count) ,
            'startValidity'          => $this->startValidity,
            'endValidity'            => $this->endValidity,
            'status'                 => $this->status,
            'createdAt'              => $createdAt,
            'updatedAt'              => $updatedAt
        ];
    }
}
