<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'client';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'client_name',
        'address1',
        'address2',
        'city',
        'state',
        'country',
        'latitude',
        'longitude',
        'phone_no1',
        'phone_no2',
        'zip',
        'start_validity',
        'end_validity',
        'status',
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User','client_id','id'); 
    }

    public function activeUser()
    {
        return $this->user->where('status','=','Active')->get()->count() ? $this->user->where('status','=','Active')->get()->count() : 0; 
    }

    // public function user()
    // {
    //     return $this->hasMany('App\Models\User','client_id','id'); 
    // }

    public function geUserTotalCountAttribute()
    {        
        $total_user = $this->user->count() ? $this->user->count() : 0; 
        $active_user = $this->user->where('status','=','Active')->count() ? $this->user->where('status','=','Active')->count() : 0;  
        $inactive_user = $this->user->where('status','=','Inactive')->count() ? $this->user->where('status','=','Inactive')->count() : 0; 
        return array($total_user,$active_user,$inactive_user); 
    } 

    public function geUserActiveCountAttribute()
    {        
        return $this->user->where('status','=','Active')->count() ? $this->user->where('status','=','Active')->count() : 0; 
    } 

    public function geUserInctiveCountAttribute()
    {        
        return $this->user->where('status','=','Inactive')->count() ? $this->user->where('status','=','Inactive')->count() : 0; 
    } 
}
