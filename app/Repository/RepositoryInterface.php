<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function getAll($attributes);
    public function getById($id);
    public function create($attributes);
    public function update($id,$attributes); 
    public function delete($id);
}