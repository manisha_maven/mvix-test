<?php

namespace App\Repository\Client;
 
use App\Exceptions\GeneralException;
use App\Models\Client;
use App\Models\User;

use function PHPUnit\Framework\lessThanOrEqual;

class ClientRepository
{
    private $client, $user;

    public function __construct(Client $client, User $user)
    {
        $this->client = $client;
        $this->user = $user;
    } 


    public function getAll($attributes)
    {  
        $sortField = $attributes['sort_field'];
        $sortType = $attributes['sort_type'];

        $search_field = $attributes['search_field']; 
        $search_value = $attributes['search_value']; 
        
        $data = $this->client
            ->select('id','client_name','address1','address2','city','state','country','zip as zipcode','latitude','longitude','phone_no1 as phoneNo1', 'phone_no2 as phoneNo2', 'start_validity as startValidity','end_validity as endValidity','status','created_at as createdAt','updated_at as updatedAt')
            ->withCount(['user','user as active_count' => function ($query) {
                $query->where('status','=','Active');
            },'user as inactive_count' => function ($query) {
                $query->where('status','=','Inactive');
            }])
            ->orderBy($sortField, $sortType);
        if(!empty($search_field) && !empty($search_value))
        {
            $data->where($search_field, 'LIKE', '%' . $search_value . '%');
        }
        
        $result = $data->paginate($attributes['per_page']);  
        return $result;
    }

    public function create($attributes)
    {
        if(empty($attributes)){
            throw new GeneralException('No Params Found');
        }
        
        $client_email_details = $this->getClientByEmail($attributes['client_email']);
        if($client_email_details['status'] == 'exist')
        {
            $client_id = $client_email_details['data']['id'];

            $phoneNo2 = '';
            if($client_email_details['data']['phone_no2'] != null && $client_email_details['data']['phone_no2'] != '')
            {
                    $phoneNo2 = $client_email_details['data']['phone_no2'];
            }

            $result_data['name'] = $client_email_details['data']['client_name'];
            $result_data['address1'] = $client_email_details['data']['address1'];
            $result_data['address2'] = $client_email_details['data']['address2'];
            $result_data['city'] = $client_email_details['data']['city'];
            $result_data['state'] = $client_email_details['data']['state'];
            $result_data['country'] = $client_email_details['data']['country'];
            $result_data['zipCode'] = $client_email_details['data']['zip'];
            $result_data['phoneNo1'] = $client_email_details['data']['phone_no1'];
            $result_data['phoneNo2'] = $phoneNo2;
        }
        else
        {
            $client = new $this->client;
            $client->client_name = $attributes['client_name'];
            $client->client_email = $attributes['client_email']; 
            $client->address1 = $attributes['client_address1'];
            $client->address2 = $attributes['client_address2'];
            $client->city = $attributes['client_city'];
            $client->state = $attributes['client_state'];
            $client->country = $attributes['client_country'];
            $client->latitude = $attributes['latitude'];
            $client->longitude = $attributes['longitude'];
            $client->zip = $attributes['client_zipcode'];
            $client->phone_no1 = $attributes['client_phone_no1'];
            $client->phone_no2 = $attributes['client_phone_no2'];
            $client->status = $attributes['client_status'];
            $client->start_validity = $attributes['start_validity'];
            $client->end_validity = $attributes['end_validity'];
            $client->save();

            $client_id = $client->id;
            $result_data['name'] = $attributes['client_name'];
            $result_data['address1'] = $attributes['client_address1'];
            $result_data['address2'] = $attributes['client_address2'];
            $result_data['city'] = $attributes['client_city'];
            $result_data['state'] = $attributes['client_state'];
            $result_data['country'] = $attributes['client_country'];
            $result_data['zipCode'] =  $attributes['client_zipcode'];
            $result_data['phoneNo1'] = $attributes['client_phone_no1'];
            $result_data['phoneNo2'] = $attributes['client_phone_no2'];
        } 

        $user = new $this->user;
        $user->client_id = $client_id;
        $user->first_name = $attributes['user_firstname']; 
        $user->last_name = $attributes['user_lastname'];
        $user->email = $attributes['user_email'];
        $user->password = $attributes['user_password'];
        $user->phone = $attributes['user_phone'];
        $user->status = $attributes['user_status'];
        $user->last_password_reset = now();
        $user->save(); 
        $result_data['users'] = array(
            "firstName" => $attributes['user_firstname'],
            "lastName"  => $attributes['user_lastname'],
            "email"     => $attributes['user_email'],
            "password"  => $attributes['user_password'],
            "phone"     => $attributes['user_phone']
        );

        return $result_data;
    }   

    public function getClientByEmail($client_email)
    {
        $data = $this->client->select('id','client_name','address1','address2','city','state','country','zip','phone_no1','phone_no2')->where('client_email', $client_email)->first();
        if (empty($data)) { 
            $error['status'] = 'not_exist'; 
            $error['message'] = 'Client does not exist';
            return $error;
        } 
        $error['status'] = 'exist'; 
        $error['data'] = $data->toArray();
        return $error;
    }
}